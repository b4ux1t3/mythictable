Welcome to Mythic Table! As a community-driven open-source project, your contributions are what keep us running. If you want to be a part of the fun, we have just a few guidelines to follow.

## Table of Contents

0. [Table of Contents](table-of-contents)
0. [Types of contributions](types-of-contributions)
0. [Ground rules](ground-rules)
0. [Build checks](build-checks)
0. [Environment setup](environment-setup)
0. [Communication](communication)

## Types of contributions

Mythic Table is an ambitious project with room for front-end and back-end developers, artists, copy-editors, and more.

* Coding new features. Check out the "Features" tag in the Issues list to see what's slated for development.
* Playtesting. If you run into a bug, search the Issues list to see if it's been reported yet - if not, let us know!
* Bug-stomping. Check out the "Bugs" tag in the Issues list if you're not sure where to start.
* Documentation. From fixing typos to writing tutorials. Check out the "Documentation" tag in the Issues list.

## Ground rules

A healthy community needs a common set of expectations. Here's what we're looking for from contributors:

* Civility. Our preferences are shaped by our individual experiences and background. Be polite, listen to understand, and consider what's best for the project and the community.
* Minimum effort. Before you submit an issue, search to see if it's already been reported. If you submit a bug, fill out the requested details.
* Test coverage. Make sure your pull requests pass the build checks, and that your contribution has test coverage. We highly encourage test driven development!
* Initiative. For minor issues like typos, feel free to submit a pull request directly; for larger features/bugs, submit an issue first for discussion.

For more specific details, check out the [Code of Conduct](CODE_OF_CONDUCT.md).

## Build checks

To maintain code quality, all pull requests must pass the following build checks. These are run automatically by GitLab, so if you see your pull request's build is failing, please make sure to update it to fix any flagged issues.

* Test coverage meets minimum requirements.
* Linting passes.
* Build artifacts are successfully generated.

## Environment setup

See DEVELOPMENT.md for details on setting up your dev environment.

## Communication

Discussions around features, bugs, and project planning take place in the Issues tracker and in the Slack channel. All are welcome to join the conversation!

* [Issues tracker](https://gitlab.com/mfaulise/mythictable/issues)
* [Slack channel](https://mythictable.slack.com/)