﻿using Newtonsoft.Json.Linq;

namespace MythicTable.Collections.Providers
{
    public static class CollectionExtensions
    {
        public static string GetId(this JObject obj)
        {
            return obj["_id"].ToString();
        }

        public static string GetPath(this JObject obj)
        {
            return obj["_path"]?.Value<string>() ?? null;
        }

        public static string GetCampaignId(this JObject obj)
        {
            return obj["_campaign"]?.Value<string>() ?? null;
        }

        public static string GetCollection(this JObject obj)
        {
            return obj["_collection"]?.Value<string>() ?? null;
        }

        public static bool IsFolder(this JObject obj)
        {
            return obj["_collection"]?.Value<string>() == "folder";
        }

        public static string GetName(this JObject obj)
        {
            return obj["_name"]?.Value<string>() ?? null;
        }

        public static void SetName(this JObject obj, string name)
        {
            obj["_name"] = name;
        }
    }
}
