﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.IdentityModel.Tokens;
using MythicTable.Campaign.Data;
using MythicTable.Collections.Providers;
using MythicTable.Common.Controllers;
using MythicTable.Profile.Data;
using Newtonsoft.Json.Linq;

namespace MythicTable.Library.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class LibraryController: AuthorizedController
    {
        public static readonly string Campaigns = "campaigns";
        public static readonly string Library = "library";
        public static readonly string Public = "public";

        private readonly ICampaignProvider campaignProvider;
        private readonly ICollectionProvider collectionProvider;

        public LibraryController(ICampaignProvider campaignProvider,
            ICollectionProvider collectionProvider,
            IProfileProvider profileProvider,
            IMemoryCache memoryCache)
             : base(profileProvider, memoryCache)
        {
            this.campaignProvider = campaignProvider;
            this.collectionProvider = collectionProvider;
        }

        // GET: api/library/ls
        [HttpGet("ls")]
        public async Task<List<LibraryDto>> Get(
            [FromQuery] string path,
            [FromQuery] List<string> types = null, 
            [FromQuery] List<string> exclude = null,
            [FromQuery] bool recursive = false)
        {
            var userId = await this.GetProfileId();
            if (path == "/")
            {
                return await GetRootFolders(types, exclude, recursive);
            }

            var libPath = new LibraryPath(path);

            if (libPath.Root == LibraryPath.RootFolder.Public)
            {
                return new List<LibraryDto>();
            }

            if (libPath.IsCampaignRoot)
            {
                var allCampaigns = await campaignProvider.GetAll(userId);
                if (!recursive)
                {
                    return (from c in allCampaigns select new LibraryDto
                    {
                        Name = LibraryPath.SlugifyCampaignName(c.Name)
                    }).ToList();
                }
                var libraryDtos = await Task.WhenAll(
                    allCampaigns.Select(
                        async c => new LibraryDto
                        {
                            Name = LibraryPath.SlugifyCampaignName(c.Name),
                            Children = await Get($"/campaigns/{LibraryPath.SlugifyCampaignName(c.Name)}", types, exclude, true)
                        }));
                return libraryDtos.ToList();
            }

            return await GetLibrary(userId, await GetCampaignId(libPath, userId), libPath.Folders, types, exclude, recursive);
        }

        // POST: api/library/mkdir
        [HttpPost("mkdir")]
        public async Task<LibraryDto> MakeFolder([FromQuery] string path, [FromQuery] string folder)
        {
            var userId = await this.GetProfileId();
            if (!IsValidFolderName(folder))
            {
                throw new LibraryException($"Invalid folder name '{folder}'");
            }

            if (FolderExists(folder, await Get(path, types: new List<string>(){"folder"})))
            {
                throw new LibraryException($"Folder '{folder}' exists at '{path}'");
            }

            var libPath = new LibraryPath(path);

            if (libPath.IsCampaignRoot)
            {
                throw new LibraryException("Cannot add folder at campaign level. Create a new campaign instead.");
            }

            return await CreateLibraryFolder(userId, await GetCampaignId(libPath, userId), folder, libPath.Folders);
        }

        // PUT: api/library/mv
        [HttpPut("mv")]
        public async Task<bool> Move([FromQuery] string id, [FromQuery] string path)
        {
            var userId = await GetProfileId();

            if (path == "/")
            {
                throw new LibraryException("Cannot move objects to root");
            }

            var libPath = new LibraryPath(path);

            if (libPath.IsCampaignRoot)
            {
                throw new LibraryException("Cannot move objects to campaign level");
            }

            var campaignId = await GetCampaignId(libPath, userId);

            if (path.EndsWith("/"))
            {
                return await MoveObject(id, campaignId, libPath.Folders) == 1;
            }
            return await MoveObject(id, campaignId, libPath.Folders.SkipLast(1).ToList(), libPath.Folders.Last()) == 1;

        }


        // POST: api/library/cp
        [HttpPost("cp")]
        public async Task<LibraryDto> Copy([FromQuery] string id, [FromQuery] string path)
        {
            var userId = await GetProfileId();

            if (path == "/")
            {
                throw new LibraryException("Cannot copy objects to root");
            }

            var libPath = new LibraryPath(path);

            if (libPath.IsCampaignRoot)
            {
                throw new LibraryException("Cannot copy objects to campaign level");
            }

            var campaignId = await GetCampaignId(libPath, userId);

            if (path.EndsWith("/"))
            {
                return new LibraryDto(await CopyObject(id, campaignId, libPath.Folders));
            }
            return new LibraryDto(await CopyObject(id, campaignId, libPath.Folders.SkipLast(1).ToList(), libPath.Folders.Last()));
        }

        // DELETE: api/library/rm
        [HttpDelete("rm")]
        public async Task<bool> Delete([FromQuery] string id)
        {
            var userId = await GetProfileId();
            var obj = await collectionProvider.Get(userId, id);
            if (obj == null)
            {
                throw new LibraryException($"Cannot delete '{id}'");
            }
            if (obj.IsFolder())
            {
                throw new LibraryException("Cannot delete folder with this method");
            }

            return await collectionProvider.Delete(userId, obj.GetCollection(), obj.GetId());
        }

        // DELETE: api/library/rmdir
        [HttpDelete("rmdir")]
        public async Task<List<LibraryDto>> DeleteFolder([FromQuery] string path)
        {
            var libPath = new LibraryPath(path);
            if (libPath.Folders.IsNullOrEmpty())
            {
                throw new LibraryException($"Cannot delete folder '{path}'");
            }

            var folderName = libPath.Folders.Last();
            var parentPath = libPath.GetParentPath();

            var parentContents = await Get(parentPath, types: null, exclude: null);
            
            var folder = parentContents.First(dto => dto.IsFolder && dto.Name == folderName);
            
            var userId = await GetProfileId();
            await collectionProvider.Delete(userId, "folder", folder.Id);

            var children = await Get(path, types: null, exclude: null);
            foreach (var child in children)
            {
                if (child.IsFolder)
                {
                    await DeleteFolder(path.TrimEnd('/') + "/" + child.Name);
                }
                else
                {
                    await Delete(child.Id);
                }
            }
            return parentContents.Where(dto => dto.Id != folder.Id).ToList();
        }

        public static bool IsValidFolderName(string name)
        {
            return Regex.IsMatch(name, $"^[{LibraryPath.ValidChars}]+$");
        }

        private async Task<List<LibraryDto>> GetLibrary(string userId, string campaignId, IReadOnlyCollection<string> folders, IReadOnlyCollection<string> types = null, IReadOnlyCollection<string> exclude = null, bool recursive = false)
        {
            var path = GetPath(folders);
            var objects = await collectionProvider.GetUserObjectByPath(userId, campaignId, path, types, exclude);
            var dtos = objects.IsNullOrEmpty() ? new List<LibraryDto>() : (from obj in objects select new LibraryDto(obj)).ToList();
            if (!recursive)
            {
                return dtos;
            }
            foreach (var dto in dtos)
            {
                var foldersToChildren = new List<string>(folders) { dto.Name };
                dto.Children = await GetLibrary(userId, campaignId, foldersToChildren, types, exclude, true);
            }
            return dtos;
        }

        private async Task<LibraryDto> CreateLibraryFolder(string userId, string campaignId, string folderName, IReadOnlyCollection<string> folders)
        {
            if (!await ExistsParentFolder(userId, campaignId, folders))
            {
                await CreateParentFolder(userId, campaignId, folders);
            }

            // TODO #670 - Removed reference to "_name"
            var folder = new JObject{{"_name", folderName}};
            if(campaignId == null)
            {
                return new LibraryDto(await collectionProvider.Create(userId, "folder", folder, string.Join("/", folders)));
            }
            return new LibraryDto(await collectionProvider.CreateByCampaign(userId, "folder", campaignId, folder, string.Join("/", folders)));
        }

        private async Task<long> MoveObject(string objectId, string campaignId, IReadOnlyCollection<string> folders, string name = null)
        {
            var userId = await this.GetProfileId();

            var obj = await collectionProvider.Get(userId, objectId);
            if (obj == null)
            {
                throw new LibraryException($"Cannot find object '{objectId}' for user '{userId}'");
            }

            if (obj.IsFolder() && LibraryPath.IsRecursive(obj, campaignId, GetPath(folders)))
            {
                throw new LibraryException("Cyclic Moving is not allowed");
            }

            ValidateChanges(obj, campaignId, folders, name);
            
            await ValidateFolderName(userId, obj, name ?? obj.GetName(), campaignId, folders);

            if (!await ExistsParentFolder(userId, campaignId, folders))
            {
                await CreateParentFolder(userId, campaignId, folders);
            }

            return await MoveRecursively(userId, objectId, name, campaignId, folders);
        }

        private async Task<long> MoveRecursively(string userId, string objectId, string name, string campaignId,
            IReadOnlyCollection<string> folders)
        {
            var obj = await collectionProvider.Get(userId, objectId);

            long result;
            if (name.IsNullOrEmpty())
            {
                result = await collectionProvider.Move(userId, obj.GetId(), campaignId, GetPath(folders));
            }
            else
            {
                result = await collectionProvider.Rename(userId, obj.GetId(), name, campaignId, GetPath(folders));
            }

            var childrenPath = new List<string>(folders) { name ?? obj.GetName() };
            await HandleChildren(obj, userId, obj.GetCampaignId(), async (id) => await MoveRecursively(userId, id, null, campaignId, childrenPath));
            return result;
        }

        private async Task<JObject> CopyObject(string objectId, string campaignId, IReadOnlyCollection<string> folders, string name = null)
        {
            var userId = await this.GetProfileId();
            var obj = await collectionProvider.Get(userId, objectId);
            if (obj == null)
            {
                throw new LibraryException($"Cannot find object '{objectId}' for user '{userId}'");
            }

            var path = GetPath(folders);
            if (obj.IsFolder() && LibraryPath.IsRecursive(obj, campaignId, path))
            {
                throw new LibraryException("Cyclic copying is not allowed");
            }

            if (!await ExistsParentFolder(userId, campaignId, folders))
            {
                await CreateParentFolder(userId, campaignId, folders);
            }

            return await CopyRecursively(userId, objectId, campaignId, folders, name);
        }

        private async Task<JObject> CopyRecursively(string userId, string objectId, string campaignId,
            IReadOnlyCollection<string> folders, string name = null)
        {
            var obj = await collectionProvider.Get(userId, objectId);
            var path = GetPath(folders);
            var clone = (JObject)obj.DeepClone();

            if (name != null)
            {
                clone.SetName(name);
            }

            if (campaignId == null)
            {
                // TODO #670 - Fix this reference to _campaign
                clone.Remove("_campaign");
            }
            JObject results;
            if (campaignId != null)
            {
                results = await collectionProvider.CreateByCampaign(userId, clone.GetCollection(), campaignId,
                    clone, path);
            }
            else
            {
                results = await collectionProvider.Create(userId, clone.GetCollection(), clone, path);
            }

            var childrenPath = new List<string>(folders) { results.GetName() };
            await HandleChildren(obj, userId, obj.GetCampaignId(), async (id) => await CopyRecursively(userId, id, campaignId, childrenPath));

            return results;
        }

        private async Task<string> GetCampaignId(LibraryPath libPath, string userId)
        {
            string campaignId = null;
            if (libPath.Campaign != null)
            {
                var allCampaigns = await campaignProvider.GetAll(userId);
                campaignId = FindCampaignId(allCampaigns, libPath.Campaign);
            }

            return campaignId;
        }

        private static string FindCampaignId(List<CampaignDTO> allCampaigns, string campaignName)
        {
            var campaign = allCampaigns.FirstOrDefault(campaign => LibraryPath.SlugifyCampaignName(campaign.Name) == campaignName);
            if (campaign == null)
            {
                throw new LibraryException($"Cannot find campaign '{campaignName}'");
            }

            return campaign.Id;
        }

        private async Task<bool> ExistsParentFolder(string userId, string campaignId, IReadOnlyCollection<string> folders)
        {
            if (folders.IsNullOrEmpty())
            {
                return true;
            }
            var parent = folders.Last();
            var parentsFolder = folders.SkipLast(1);
            var objects = await GetLibrary(userId, campaignId, parentsFolder.ToList());
            return objects.Any(o => o.Name == parent && o.IsFolder);
        }

        private async Task CreateParentFolder(string userId, string campaignId, IReadOnlyCollection<string> folders)
        {
            var parent = folders.Last();
            var parentsFolder = folders.SkipLast(1);
            await CreateLibraryFolder(userId, campaignId, parent, parentsFolder.ToList());
        }

        private async Task HandleChildren(JObject original, string userId, string campaignId,
            Func<string, Task> func)
        {
            if (original.IsFolder())
            {
                var selfPath = original.GetPath() != null ? original.GetPath().Split("/").ToList() : new List<string>();
                selfPath.Add(original.GetName());
                var children = await GetLibrary(userId, campaignId, selfPath);
                foreach (var child in children)
                {
                    await func(child.Id);
                }
            }
        }

        private static void ValidateChanges(JObject obj, string campaignId, IReadOnlyCollection<string> folders, string name = null)
        {
            if (obj.GetCampaignId() == campaignId &&
                obj.GetPath() == GetPath(folders) &&
                (name == null || obj.GetName() == name))
            {
                throw new LibraryException($"Cannot move '{obj.GetId()}'. No changes.");
            }
        }

        private async Task ValidateFolderName(string userId, JObject obj, string name, string campaignId, IReadOnlyCollection<string> folders)
        {
            if (obj.IsFolder() && FolderExists(name, await GetLibrary(userId, campaignId, folders, new List<string>(){"folder"})))
            {
                throw new LibraryException($"Cannot rename folder because '{name}' exists at '{GetPath(folders)}'");
            }
        }

        private static string GetPath(IReadOnlyCollection<string> folders)
        {
            var path = string.Join("/", folders);
            return path.IsNullOrEmpty() ? null : path;
        }

        private static bool FolderExists(string folderName, IReadOnlyCollection<LibraryDto> results)
        {
            return !results.IsNullOrEmpty() && results.Any(o => o.Name == folderName);
        }

        private async Task<List<LibraryDto>> GetRootFolders(List<string> types, List<string> exclude, bool recursive = false)
        {
            if (!recursive)
            {
                return LibraryPath.RootFolders.Select(s => new LibraryDto { Name = s }).ToList();
            }

            var libraryDtos = await Task.WhenAll(
                LibraryPath.RootFolders.Select(
                    async s => new LibraryDto
                    {
                        Name = s,
                        Children = await Get($"/{s}", types, exclude, true)
                    }));
            return libraryDtos.ToList();
        }
    }
}