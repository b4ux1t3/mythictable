﻿using System;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Moq;
using MythicTable.Campaign.Data;
using MythicTable.Collections.Providers;
using MythicTable.Library.Controller;
using MythicTable.Profile.Data;
using MythicTable.TestUtils.Profile.Util;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;
using Xunit;
using MythicTable.Common.Exceptions;
using System.Collections.Generic;
using System.Linq;

namespace MythicTable.Integration.Tests.Library.Controller
{
    public class LibraryControllerTests: TestCollection
    {
        public Mock<ILogger<MongoDbCollectionProvider>> LoggerMock;
        protected ICampaignProvider CampaignProvider;
        protected ICollectionProvider CollectionProvider;
        protected string User { get; set; } = "Jon";
        protected string UserId { get; set; }

        protected LibraryController Controller;

        public override async Task InitializeAsync()
        {
            LoggerMock = new Mock<ILogger<MongoDbCollectionProvider>>();
            await base.InitializeAsync();
            CollectionProvider = new MongoDbCollectionProvider(mongoSettings, mongoClient, LoggerMock.Object);
            CampaignProvider = new MongoDbCampaignProvider(mongoSettings, mongoClient);
            var profileProviderMock = new Mock<IProfileProvider>();
            var profile = ProfileTestUtil.CreateProfile(profileProviderMock, User);
            UserId = profile.Id;

            Controller = new LibraryController(CampaignProvider, CollectionProvider, profileProviderMock.Object, new MemoryCache(new MemoryCacheOptions()));

            var mockHttpContext = new Mock<HttpContext>();
            mockHttpContext.Setup(hc => hc.User.FindFirst(It.IsAny<string>()))
                .Returns(() => new Claim("", User));
            Controller.ControllerContext.HttpContext = mockHttpContext.Object;
        }


        [Fact]
        public async Task ObjectMustExist()
        {
            const string objectId = "1";
            var exception = await Assert.ThrowsAsync<MythicTableException>(() => Controller.Move(objectId, "/library/"));
            Assert.Equal($"Could not find item '{objectId}' for user '{UserId}'", exception.Message);
        }

        [Fact]
        public async Task MakesAllFoldersRecursively()
        {
            const string folderName = "folder-name";
            await Controller.MakeFolder("/library/path/to/", folderName);

            var jObjects = await CollectionProvider.GetList(UserId, "folder");
            Assert.Equal(3, jObjects.Count);
            Assert.Equal("path", jObjects[0]["_name"]);
            Assert.Equal("to", jObjects[1]["_name"]);
            Assert.Equal(folderName, jObjects[2]["_name"]);
        }

        [Fact]
        public async Task MovingCreatesFolders()
        {
            var foo = await CollectionProvider.Create(UserId, "character", new JObject { { "Name", "foo" } });
            Assert.Null(foo.GetPath());

            await Controller.Move(foo.GetId(), "/library/path/to/");

            var jObject = await CollectionProvider.Get(UserId, foo.GetId());
            Assert.Equal(foo.GetId(), jObject.GetId());
            Assert.Equal("path/to", jObject.GetPath());

            var jObjects = await CollectionProvider.GetList(UserId, "folder");
            Assert.Equal(2, jObjects.Count);
            Assert.Equal("path", jObjects[0]["_name"]);
            Assert.Equal("to", jObjects[1]["_name"]);
        }

        [Fact]
        public async Task MoveChangesCampaign()
        {
            var campaign1 = await CampaignProvider.Create(new CampaignDTO{ Name = "campaign1" }, UserId);
            var campaign2 = await CampaignProvider.Create(new CampaignDTO { Name = "campaign2" }, UserId);
            var foo = await CollectionProvider.CreateByCampaign(UserId, "character", campaign1.Id, new JObject { { "Name", "foo" } });

            await Controller.Move(foo.GetId(), "/campaigns/campaign2/");

            var jObject = await CollectionProvider.Get(UserId, foo.GetId());
            Assert.Equal(foo.GetId(), jObject.GetId());
            Assert.Equal(campaign2.Id, jObject.GetCampaignId());
        }

        [Fact]
        public async Task ExhaustiveTest()
        {
            var campaign1 = await CampaignProvider.Create(new CampaignDTO { Name = "campaign1" }, UserId);
            var campaign2 = await CampaignProvider.Create(new CampaignDTO { Name = "campaign2" }, UserId);

            var folder1 = await CollectionProvider.CreateByCampaign(UserId, "folder", campaign1.Id, new JObject
            {
                { "_name", "folder1" }
            });
            await CollectionProvider.CreateByCampaign(UserId, "folder", campaign1.Id, new JObject
            {
                { "_name", "folder2" },
                { "_path", "folder1" }
            });

            var char1 = await CollectionProvider.CreateByCampaign(UserId, "character", campaign1.Id, new JObject
            {
                { "Name", "char1" }
            });
            await CollectionProvider.CreateByCampaign(UserId, "character", campaign1.Id, new JObject
            {
                { "Name", "char2" },
                { "_path", "folder1" }
            });
            await CollectionProvider.CreateByCampaign(UserId, "character", campaign1.Id, new JObject
            {
                { "Name", "char3" },
                { "_path", "folder1/folder2" }
            });

            await AssetStartingPoint();
            var libraryFolder = await TestCopyingToLibrary(folder1);
            await TestCopyingObjects(char1);
            await TestCopyingFolders(folder1);
            await TestCyclicCopying(folder1);
            await TestMoveToLibrary(libraryFolder);
            await TestDeleteCharacter();
            await TestDeleteFolder(campaign2);
            await TestFiltering();
        }
        private async Task AssetStartingPoint()
        {
            ResponseContains(await Controller.Get("/campaigns/"), 
                new List<Action<LibraryDto>> {
                    (dto) => Assert.Equal("campaign1", dto.Name),
                    (dto) => Assert.Equal("campaign2", dto.Name)
                });

            ResponseContains(await Controller.Get("/campaigns/campaign1"),
                new List<Action<LibraryDto>> {
                    (dto) => Assert.Equal("folder1", dto.Name),
                    (dto) => Assert.Equal("char1", dto.Name)
                });

            ResponseContains(await Controller.Get("/campaigns/campaign1/folder1"),
                new List<Action<LibraryDto>> {
                    (dto) => Assert.Equal("folder2", dto.Name),
                    (dto) => Assert.Equal("char2", dto.Name)
                });

            ResponseContains(await Controller.Get("/campaigns/campaign1/folder1/folder2"),
                new List<Action<LibraryDto>> {
                    (dto) => Assert.Equal("char3", dto.Name)
                });
        }

        private async Task<LibraryDto> TestCopyingToLibrary(JObject folder1)
        {
            var libraryFolder = await Controller.Copy(folder1.GetId(), "/library/");
            Assert.Null(libraryFolder.Campaign);
            Assert.NotEqual(folder1.GetId(), libraryFolder.Id);
            return libraryFolder;
        }

        private async Task TestCopyingObjects(JObject char1)
        {
            var char4 = await Controller.Copy(char1.GetId(), "/campaigns/campaign1/folder1/");
            Assert.NotEqual(char1.GetId(), char4.Id);

            ResponseContains(await Controller.Get("/campaigns/campaign1/folder1"),
                new List<Action<LibraryDto>> {
                    (dto) => Assert.Equal("folder2", dto.Name),
                    (dto) => Assert.Equal("char2", dto.Name),
                    (dto) => Assert.Equal("char1", dto.Name)
                });

            ResponseContains(await Controller.Get("/campaigns/campaign2"),
                new List<Action<LibraryDto>>());

            await Controller.Copy(char1.GetId(), "/campaigns/campaign2/");

            ResponseContains(await Controller.Get("/campaigns/campaign2"),
                new List<Action<LibraryDto>>{
                    (dto) => Assert.Equal("char1", dto.Name)

                });
        }

        private async Task TestCopyingFolders(JObject folder1)
        {
            await Controller.Copy(folder1.GetId(), "/campaigns/campaign1/folder3");

            ResponseContains(await Controller.Get("/campaigns/campaign1"),
                new List<Action<LibraryDto>> {
                    (dto) => Assert.Equal("folder1", dto.Name),
                    (dto) => Assert.Equal("char1", dto.Name),
                    (dto) => Assert.Equal("folder3", dto.Name)
                });

            await Controller.Copy(folder1.GetId(), "/campaigns/campaign2/");

            ResponseContains(await Controller.Get("/campaigns/campaign2"),
                new List<Action<LibraryDto>>{
                    (dto) => Assert.Equal("char1", dto.Name),
                    (dto) => Assert.Equal("folder1", dto.Name)
                });

            ResponseContains(await Controller.Get("/campaigns/campaign2/folder1"),
                new List<Action<LibraryDto>> {
                    (dto) => Assert.Equal("folder2", dto.Name),
                    (dto) => Assert.Equal("char2", dto.Name),
                    (dto) => Assert.Equal("char1", dto.Name)
                });

            ResponseContains(await Controller.Get("/campaigns/campaign2/folder1/folder2"),
                new List<Action<LibraryDto>> {
                    (dto) => Assert.Equal("char3", dto.Name)
                });
        }

        private async Task TestCyclicCopying(JObject folder1)
        {
            var exception = await Assert.ThrowsAsync<LibraryException>(() => Controller.Copy(folder1.GetId(), "/campaigns/campaign1/folder1/folder2/"));
            Assert.Equal("Cyclic copying is not allowed", exception.Message);
        }

        private async Task TestMoveToLibrary(LibraryDto libraryFolder)
        {
            ResponseContains(await Controller.Get("/library"),
                new List<Action<LibraryDto>> {
                    (dto) => Assert.Equal("folder1", dto.Name),
                });

            ResponseContains(await Controller.Get("/library/folder1"),
                new List<Action<LibraryDto>> {
                    (dto) => Assert.Equal("folder2", dto.Name),
                    (dto) => Assert.Equal("char2", dto.Name)
                });

            var results = await Controller.Move(libraryFolder.Id, "/library/moved");
            Assert.True(results);

            ResponseContains(await Controller.Get("/library"),
                new List<Action<LibraryDto>> {
                    (dto) => Assert.Equal("moved", dto.Name),
                });

            ResponseContains(await Controller.Get("/library/moved"),
                new List<Action<LibraryDto>> {
                    (dto) => Assert.Equal("folder2", dto.Name),
                    (dto) => Assert.Equal("char2", dto.Name)
                });
        }

        private async Task TestDeleteCharacter()
        {
            var libraryMovedContents = await Controller.Get("/library/moved");
            ResponseContains(libraryMovedContents,
                new List<Action<LibraryDto>> {
                    (dto) => Assert.Equal("folder2", dto.Name),
                    (dto) => Assert.Equal("char2", dto.Name)
                });

            var char2InLibrary = libraryMovedContents[1];
            var numDeleted = await CollectionProvider.Delete(UserId, char2InLibrary.Collection, char2InLibrary.Id);
            Assert.True(numDeleted);

            ResponseContains(await Controller.Get("/library/moved"),
                new List<Action<LibraryDto>> {
                    (dto) => Assert.Equal("folder2", dto.Name)
                });
        }

        private async Task TestDeleteFolder(CampaignDTO campaign2)
        {
            ResponseContains(await Controller.Get("/campaigns/campaign2/folder1"),
                new List<Action<LibraryDto>> {
                    (dto) => Assert.Equal("folder2", dto.Name),
                    (dto) => Assert.Equal("char2", dto.Name),
                    (dto) => Assert.Equal("char1", dto.Name)
                });

            ResponseContains(await Controller.Get("/campaigns/campaign2"),
                new List<Action<LibraryDto>> {
                    (dto) => Assert.Equal("char1", dto.Name),
                    (dto) => Assert.Equal("folder1", dto.Name)
                });

            var characters = await CollectionProvider.GetListByCampaign("character", campaign2.Id);
            Assert.Equal(4, characters.Count);

            var remainingContents = await Controller.DeleteFolder("/campaigns/campaign2/folder1");
            Assert.Single(remainingContents);


            ResponseContains(await Controller.Get("/campaigns/campaign2"),
                new List<Action<LibraryDto>> {
                    (dto) => Assert.Equal("char1", dto.Name)
                });

            characters = await CollectionProvider.GetListByCampaign("character", campaign2.Id);
            Assert.Single(characters);
        }

        private async Task TestFiltering()
        {
            var campaign = await CampaignProvider.Create(new CampaignDTO { Name = "filtering" }, UserId);

            await CollectionProvider.CreateByCampaign(UserId, "folder", campaign.Id, new JObject
            {
                { "_name", "folder1" }
            });

            await CollectionProvider.CreateByCampaign(UserId, "character", campaign.Id, new JObject
            {
                { "Name", "filtering-char" }
            });

            await CollectionProvider.CreateByCampaign(UserId, "map", campaign.Id, new JObject
            {
                { "Name", "filtering-map" }
            });

            ResponseContains(await Controller.Get("/campaigns/filtering"),
                new List<Action<LibraryDto>> {
                    (dto) => Assert.Equal("folder1", dto.Name),
                    (dto) => Assert.Equal("filtering-char", dto.Name),
                    (dto) => Assert.Equal("filtering-map", dto.Name)
                });

            ResponseContains(await Controller.Get("/campaigns/filtering", types: new List<string>() {"folder"}),
                new List<Action<LibraryDto>> {
                    (dto) => Assert.Equal("folder1", dto.Name)
                });

            ResponseContains(await Controller.Get("/campaigns/filtering", exclude: new List<string>() {"folder"}),
                new List<Action<LibraryDto>> {
                    (dto) => Assert.Equal("filtering-char", dto.Name),
                    (dto) => Assert.Equal("filtering-map", dto.Name)
                });
        }


        private void ResponseContains<T>(List<T> results, List<Action<T>> actions)
        {
            Assert.Equal(actions.Count, results.Count);
            foreach (var (action, result) in actions.Zip(results, (i1, i2) => (i1, i2)))
            {
                action(result);
            }
        }
    }
}
