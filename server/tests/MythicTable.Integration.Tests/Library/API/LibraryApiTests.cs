using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using MythicTable.Collections.Providers;
using MythicTable.Integration.Tests.Helpers;
using MythicTable.Integration.TestUtils.Helpers;
using MythicTable.Library.Controller;
using MythicTable.TestUtils.Library;
using MythicTable.TestUtils.Profile.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;

namespace MythicTable.Integration.Tests.Library.API
{
    public class LibraryApiTests
    {
        private readonly HttpClient client;

        private const string BaseUrl = "/api/library";

        public LibraryApiTests()
        {
            var builder = new WebHostBuilder().UseStartup<TestStartup>();
            var server = new TestServer(builder);
            client = server.CreateClient();
        }

        [Fact]
        public async Task GetBaseLibrary()
        {
            await ProfileTestUtil.Login(client);
            
            var library = await Get<List<LibraryDto>>("/");
            
            Assert.Equal(3, library.Count);
            Assert.Equal("campaigns", library[0].Name);
            Assert.Equal("library", library[1].Name);
            Assert.Equal("public", library[2].Name);
        }

        [Fact]
        public async Task MakingAFolder()
        {
            await ProfileTestUtil.Login(client);

            var folder = await MakeFolder<LibraryDto>("/library", "path");
            
            Assert.Equal("path", folder.Name);
        }

        [Fact]
        public async Task MoveAFolder()
        {
            await ProfileTestUtil.Login(client);
            var folder = await MakeFolder<LibraryDto>("/library", "path");
            Assert.Equal("path", folder.Name);

            Assert.True(await Move<bool>(folder.Id, "/library/moved"));

            var library = await Get<List<LibraryDto>>("/library");
            Assert.Single(library);
            Assert.Equal("moved", library[0].Name);
        }

        [Fact]
        public async Task CopyAFolder()
        {
            await ProfileTestUtil.Login(client);
            var folder = await MakeFolder<LibraryDto>("/library", "path");
            Assert.Equal("path", folder.Name);

            var newFolder = await Copy<JObject>(folder.Id, "/library/copy");

            var library = await Get<List<LibraryDto>>("/library");
            Assert.Equal(2, library.Count);
            Assert.Equal("path", library[0].Name);
            Assert.Equal("copy", library[1].Name);
        }

        [Fact]
        public async Task DeleteObject()
        {
            await ProfileTestUtil.Login(client);

            var jObject = await CreateObjectInCollection<JObject>("character", new JObject());
            Assert.Single(await Get<List<LibraryDto>>("/library"));

            Assert.True(await Delete<bool>(jObject.GetId()));

            Assert.Empty(await Get<List<LibraryDto>>("/library"));
        }

        [Fact]
        public async Task DeleteAFolder()
        {
            await ProfileTestUtil.Login(client);
            var folder = await MakeFolder<LibraryDto>("/library", "path");
            Assert.Equal("path", folder.Name);

            await DeleteFolder<List<LibraryDto>>("/library/path");

            var library = await Get<List<LibraryDto>>("/library");
            Assert.Empty(library);
        }

        [Fact]
        public async Task FolderOnly()
        {
            await ProfileTestUtil.Login(client);
            await CreateObjectInCollection<JObject>("character", new JObject());
            await CreateObjectInCollection<JObject>("character", new JObject());
            await MakeFolder<LibraryDto>("/library", "path");

            Assert.Equal(3, (await Get<List<LibraryDto>>("/library")).Count);
            Assert.Single(await Get<List<LibraryDto>>("/library", types: new[] { "folder" }));
            Assert.Single(await Get<List<LibraryDto>>("/library", exclude: new[] { "character" }));
        }

        [Fact]
        public async Task FolderOnlyRecursive()
        {
            await ProfileTestUtil.Login(client);
            await MakeFolder<LibraryDto>("/library", "path");
            await MakeFolder<LibraryDto>("/library/path", "to");
            await MakeFolder<LibraryDto>("/library/path/to", "target");

            var results = await Get<List<LibraryDto>>("/", types: new[] { "folder" }, recursive: true);

            var expectation = JObject.Parse($@"{{
                campaigns: {{
                    Tutorial_Campaign: {{}}
                }},
                library: {{
                    path: {{
                        to: {{
                            target: {{}}
                        }}
                    }}
                }},
                public: {{}}
            }}");
            LibraryTestHelper.VerifyFolderStructure(expectation, results);
        }

        private async Task<T> Get<T>(string path, string[] types = null, string[] exclude = null, bool recursive = false)
        {
            types ??= Array.Empty<string>();
            exclude ??= Array.Empty<string>();
            var url = $"{BaseUrl}/ls?path={path}";
            url = types.Aggregate(url, (current, type) => current + $"&types={type}");
            url = exclude.Aggregate(url, (current, exclude) => current + $"&exclude={exclude}");
            if (recursive)
            {
                url += "&recursive=true";
            }
            return await Request<T>(HttpMethod.Get, url);
        }

        private async Task<T> MakeFolder<T>(string path, string folder)
        {
            return await Request<T>(HttpMethod.Post, $"{BaseUrl}/mkdir?path={path}&folder={folder}");
        }

        private async Task<T> Move<T>(string id, string path)
        {
            return await Request<T>(HttpMethod.Put, $"{BaseUrl}/mv?id={id}&path={path}");
        }

        private async Task<T> Copy<T>(string id, string path)
        {
            return await Request<T>(HttpMethod.Post, $"{BaseUrl}/cp?id={id}&path={path}");
        }

        private async Task<T> Delete<T>(string id)
        {
            return await Request<T>(HttpMethod.Delete, $"{BaseUrl}/rm?id={id}");
        }

        private async Task<T> DeleteFolder<T>(string path)
        {
            return await Request<T>(HttpMethod.Delete, $"{BaseUrl}/rmdir?path={path}");
        }

        private async Task<T> CreateObjectInCollection<T>(string type, JObject jObject)
        {
            
            return await Request<T>(HttpMethod.Post, $"/api/collections/{type}", jObject);
        }

        private async Task<T> Request<T>(HttpMethod httpMethod, string path, JObject jObject = null)
        {
            var rqInfo = new HttpRequestInfo() { Method = httpMethod, Url = BaseUrl };
            rqInfo.Url = $"{path}";
            if (jObject != null)
            {
                rqInfo.Content = jObject;
            }

            using var response = await client.MakeRequest(rqInfo);
            response.EnsureSuccessStatusCode();
            Assert.NotNull(response.Content.Headers.ContentType);
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}
