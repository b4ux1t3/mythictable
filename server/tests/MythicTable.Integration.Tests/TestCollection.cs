using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Abstractions;
using Mongo2Go;
using MongoDB.Driver;
using Xunit;

namespace MythicTable.Integration.Tests
{
    public abstract class TestCollection : IAsyncLifetime
    {
        protected MongoDbRunner runner;
        protected MongoDbSettings mongoSettings;
        protected MongoClient mongoClient;

        public virtual Task InitializeAsync()
        {
            runner = MongoDbRunner.Start(additionalMongodArguments: "--quiet", logger: NullLogger<TestCollection>.Instance);
            mongoSettings = new MongoDbSettings 
            {
                ConnectionString = runner.ConnectionString,
                DatabaseName = "mythictable"
            };
            mongoClient = new MongoClient(mongoSettings.ConnectionString);
            return Task.CompletedTask;
        }

        public virtual Task DisposeAsync()
        {
            runner.Dispose();
            return Task.CompletedTask;
        }
    }
}