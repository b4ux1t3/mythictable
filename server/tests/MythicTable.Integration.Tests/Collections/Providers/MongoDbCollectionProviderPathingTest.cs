﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Moq;
using MythicTable.Collections.Providers;
using Newtonsoft.Json.Linq;
using Xunit;

namespace MythicTable.Integration.Tests.Collections.Providers
{
    public class MongoDbCollectionProviderPathingTest : TestCollection
    {
        private const string UserId = "test-user";
        private const string CampaignId = "campaign-test";

        public Mock<ILogger<MongoDbCollectionProvider>> LoggerMock;
        private MongoDbCollectionProvider provider;

        public override async Task InitializeAsync()
        {
            LoggerMock = new Mock<ILogger<MongoDbCollectionProvider>>();
            await base.InitializeAsync();
            provider = new MongoDbCollectionProvider(mongoSettings, mongoClient, LoggerMock.Object);
        }

        [Fact]
        public async Task CanGetAllTypes()
        {
            await provider.CreateByCampaign(UserId, "character", CampaignId, new JObject());
            await provider.CreateByCampaign(UserId, "map", CampaignId, new JObject());

            var found = await provider.GetUserObjectByPath(UserId, CampaignId, null);
            Assert.Equal(2, found.Count);
        }

        [Fact]
        public async Task GetsOnlyNullPath()
        {
            const string path = "path/to/character";
            await provider.CreateByCampaign(UserId, "character", CampaignId, new JObject(), path);
            var map = await provider.CreateByCampaign(UserId, "map", CampaignId, new JObject());

            var found = await provider.GetUserObjectByPath(UserId, CampaignId, null);
            Assert.Single(found);
            Assert.Equal(map.GetId(), found[0].GetId());
        }

        [Fact]
        public async Task GetsOnlyObjectOnPath()
        {
            const string path = "path/to/character";
            const string otherPath = "path/to/other";
            var character = await provider.CreateByCampaign(UserId, "character", CampaignId, new JObject(), path);
            await provider.CreateByCampaign(UserId, "map", CampaignId, new JObject());
            var other = await provider.CreateByCampaign(UserId, "character", CampaignId, new JObject(), otherPath);

            var found = await provider.GetUserObjectByPath(UserId, CampaignId, path);
            Assert.Single(found);
            Assert.Equal(character.GetId(), found[0].GetId());

            found = await provider.GetUserObjectByPath(UserId, CampaignId, otherPath);
            Assert.Single(found);
            Assert.Equal(other.GetId(), found[0].GetId());
        }
    }
}
