﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.Extensions.Logging;
using Mongo2Go;
using MongoDB.Bson;
using MongoDB.Driver;
using Moq;
using MythicTable.Campaign.Util;
using MythicTable.Collections.Providers;
using Xunit;

namespace MythicTable.Integration.Tests.Collections.Characters
{
    public class MongoDbCharacterSerializationBugTest : TestCollection
    {
        private const string UserId = "test-user";

        public Mock<ILogger<MongoDbCollectionProvider>> LoggerMock;
        private MongoDbCollectionProvider provider;

        public override async Task InitializeAsync()
        {
            await base.InitializeAsync();
            LoggerMock = new Mock<ILogger<MongoDbCollectionProvider>>();
            provider = new MongoDbCollectionProvider(mongoSettings, mongoClient, LoggerMock.Object);
        }

        [Fact]
        public async Task CreateCollectionCharacterRequiresBson()
        {
            var results = await provider.CreateByCampaign(
                UserId,
                "characters",
                "campaignId",
                CharacterUtil.CreateCollectionCharacter(new BsonObjectId(ObjectId.GenerateNewId()), "Redcap", "Redcap", "Goblin rogue", null, null)
            );

            var found = await provider.GetListByCampaign("characters", "campaignId");
            Assert.Single(found);

            var patch = new JsonPatchDocument().Replace("/pos/r", 5);
            await provider.UpdateByCampaign("characters", "campaignId", results.GetId(), patch);
        }
    }
}
