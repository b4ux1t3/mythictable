﻿using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using MythicTable.Library.Controller;
using Newtonsoft.Json.Linq;
using Xunit;
using static Xunit.Assert;

namespace MythicTable.TestUtils.Library
{
    public class LibraryTestHelper
    {
        public static void VerifyFolderStructure(JObject expectation, IReadOnlyCollection<LibraryDto> results)
        {
            var resultNames = string.Join(", ", results.Select(r => r.Name));
            var expectationNames = string.Join(", ", expectation.Properties().Select(p => p.Name));
            Assert.Equal(expectationNames, resultNames);
            
            foreach (var (name, value) in expectation)
            {
                var childrenExpectation = (JObject)value;

                var result = results.FirstOrDefault(r => r.Name == name);
                True(result != null, $"Couldn't find '{name}'. Options are [{resultNames}].");

                if (!result.Children.IsNullOrEmpty())
                {
                    VerifyFolderStructure(childrenExpectation, result.Children);
                }
                else
                {
                    True(childrenExpectation.IsNullOrEmpty(), $"Expected '{name}' has children [{expectationNames}] but it doesn't");
                }
            }
        }
    }
}
