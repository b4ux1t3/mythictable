﻿using System.Linq;
using Moq;
using MythicTable.Library.Controller;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using Xunit;
using static Xunit.Assert;

namespace MythicTable.Tests.Library.Controller
{
    public class LibraryControllerDeleteTests : LibraryControllerBase
    {
        [Fact]
        public async Task CannotDeleteObjectThatDoesExist()
        {
            const string objectId = "1";
            var exception = await ThrowsAsync<LibraryException>(() => Controller.Delete(objectId));
            Equal($"Cannot delete '{objectId}'", exception.Message);
        }

        [Fact]
        public async Task CallsDeleteOnProvider()
        {
            const string objectId = "1";
            SetupCollectionGet(objectId, null, null, "character", new JObject());
            True(await Controller.Delete(objectId));
            CollectionProviderMock.Verify(cp => cp.Delete(
                It.IsAny<string>(),
                "character",
                objectId), Times.Once);
        }

        [Fact]
        public async Task CannotDeleteFolders()
        {
            const string objectId = "1";
            SetupCollectionGet(objectId, null, null, "folder", new JObject());
            var exception = await ThrowsAsync<LibraryException>(() => Controller.Delete(objectId));
            Equal($"Cannot delete folder with this method", exception.Message);
        }

        [Theory]
        [InlineData("/foo")]
        [InlineData("//")]
        public async Task DeleteRequiresValidPath(string path)
        {
            var exception = await ThrowsAsync<LibraryException>(() => Controller.DeleteFolder(path));
            Equal($"Invalid path '{path}'", exception.Message);
        }

        [Theory]
        [InlineData("/")]
        [InlineData("/campaigns")]
        [InlineData("/campaigns/foo")]
        [InlineData("/library")]
        public async Task CannotDeleteBuiltInPaths(string path)
        {
            var exception = await ThrowsAsync<LibraryException>(() => Controller.DeleteFolder(path));
            Equal($"Cannot delete folder '{path}'", exception.Message);
        }

        [Fact]
        public async Task DeleteFolderCallsDeleteOnProvider()
        {
            SetupCampaignCollections(
                (string)null,
                CreateObject(1, null, "foo1"),
                CreateObject(2, null, "foo2"),
                CreateFolder(3, "folder-name", null, null),
                CreateObject(4, null, "foo3")
            );
            var remainingContents = await Controller.DeleteFolder("/library/folder-name");
            Equal(3, remainingContents.Count);
            True(remainingContents.All(dto => dto.Name != "folder-name"));

            VerifyDelete("3", "folder");
        }

        [Fact]
        public async Task DeleteFolderIsRecursive()
        {
            SetupCampaignCollections(
                (string)null,
                CreateFolder(1, "folder1", null, null)
            );
            SetupCampaignCollections(
                (string)null, "folder1",
                CreateFolder(2, "folder2", null, null)
            );
            SetupCampaignCollections(
                (string)null, "folder1/folder2",
                CreateFolder(3, "folder3", null, null)
            );
            SetupCampaignCollections(
                (string)null, "folder1/folder2/folder3",
                CreateObject(4, null, "foo1")
            );
            SetupCollectionGet("4", null, null, "character", CreateObject(4, null, "foo1"));

            var remainingContents = await Controller.DeleteFolder("/library/folder1/folder2");
            Empty(remainingContents);

            VerifyDelete("2", "folder");
            VerifyDelete("3", "folder");
            VerifyDelete("4", "character");
        }
    }
}
