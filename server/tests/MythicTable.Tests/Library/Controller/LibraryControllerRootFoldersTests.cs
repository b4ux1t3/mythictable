﻿using System.Threading.Tasks;
using MythicTable.Campaign.Data;
using MythicTable.Library.Controller;
using MythicTable.TestUtils.Library;
using Newtonsoft.Json.Linq;
using Xunit;
using static Xunit.Assert;

namespace MythicTable.Tests.Library.Controller
{
    public class LibraryControllerRootFoldersTests: LibraryControllerBase
    {
        [Fact]
        public async Task ReturnsRootFolders()
        {
            var results = await Controller.Get("/");
            Equal(3, results.Count);

            Equal(LibraryController.Campaigns, results[0].Name);
            Equal(LibraryController.Library, results[1].Name);
            Equal(LibraryController.Public, results[2].Name);
        }

        [Fact]
        public async Task CampaignsReturnsEmptyList()
        {
            var results = await Controller.Get("/campaigns/");
            Empty(results);
        }


        [Fact]
        public async Task LibraryReturnsEmptyList()
        {
            var results = await Controller.Get("/library/");
            Empty(results);
        }


        [Fact]
        public async Task PublicReturnsEmptyList()
        {
            var results = await Controller.Get("/public/");
            Empty(results);
        }

        [Theory]
        [InlineData("")]
        [InlineData("//")]
        public async Task InvalidPathReturnsException(string path)
        {
            var exception = await ThrowsAsync<LibraryException>(() => Controller.Get(path));
            Equal($"Invalid path '{path}'", exception.Message);
        }

        [Fact]
        public async Task RootFolderRecursionIsMagic()
        {
            SetupCampaigns(
                new CampaignDTO { Id = "1", Name = "foo" },
                new CampaignDTO { Id = "2", Name = "bar" }
            );
            SetupCampaignCollections(
                null,
                CreateFolder(1, "folder1", null, null)
            );
            SetupCampaignCollections(
                null, "folder1",
                CreateFolder(2, "folder2", null, null)
            );
            SetupCampaignCollections(
                "1",
                CreateFolder(3, "folder3", "1", null)
            );

            var results = await Controller.Get("/", recursive: true);
            var expectation = JObject.Parse($@"{{
                campaigns: {{
                    foo: {{
                        folder3: {{
                        }},
                    }},
                    bar: {{
                    }}
                }},
                library: {{
                    folder1: {{
                        folder2: {{
                        }},
                    }},
                }},
                public: {{}}
            }}");
            LibraryTestHelper.VerifyFolderStructure(expectation, results);
        }
    }
}
