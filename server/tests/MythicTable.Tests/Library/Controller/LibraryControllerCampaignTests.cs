﻿using System.Threading.Tasks;
using MythicTable.Campaign.Data;
using MythicTable.Library.Controller;
using MythicTable.TestUtils.Library;
using Newtonsoft.Json.Linq;
using Xunit;
using static Xunit.Assert;

namespace MythicTable.Tests.Library.Controller
{
    public class LibraryControllerCampaignTests: LibraryControllerBase
    {
        [Fact]
        public async Task ReturnsEmptyList()
        {
            var results = await Controller.Get("/campaigns/");
            Empty(results);
        }

        [Fact]
        public async Task CannotFindCampaign()
        {
            var exception = await ThrowsAsync<LibraryException>(() => Controller.Get("/campaigns/foo"));
            Equal($"Cannot find campaign 'foo'", exception.Message);
        }

        [Fact]
        public async Task ReturnsCampaignNames()
        {
            SetupCampaigns(
                new CampaignDTO { Id = "1", Name = "foo" },
                new CampaignDTO { Id = "2", Name = "bar" }
            );
            var results = await Controller.Get("/campaigns/");
            Equal(2, results.Count);
            
            Equal("foo", results[0].Name);
            Equal("bar", results[1].Name);
        }


        [Fact]
        public async Task ReturnsNothingFromEmptyCampaign()
        {
            SetupCampaigns(
                new CampaignDTO { Id = "1", Name = "foo" },
                new CampaignDTO { Id = "2", Name = "bar" }
            );
            var results = await Controller.Get("/campaigns/foo");
            Empty(results);
            results = await Controller.Get("/campaigns/foo/bar");
            Empty(results);
        }


        [Fact]
        public async Task ReturnsCollectionsFromCampaign()
        {
            SetupCampaigns(
                new CampaignDTO{ Id = "1", Name = "foo" },
                new CampaignDTO{ Id = "2", Name = "bar" }
            );
            SetupCampaignCollections(
                "1",
                CreateObject(1, "1"),
                CreateObject(2, "1"),
                CreateObject(3, "1")
            );
            SetupCampaignCollections(
                "2",
                CreateObject(1, "2")
            );
            var results = await Controller.Get("/campaigns/foo");
            Equal(3, results.Count);
            results = await Controller.Get("/campaigns/bar");
            Single(results);
        }

        [Fact]
        public async Task ReturnsOnlyCollectionsInPath()
        {
            SetupCampaigns(
                new CampaignDTO{ Id = "1", Name = "campaign1" },
                new CampaignDTO{ Id = "2", Name = "campaign2" }
            );
            SetupCampaignCollections(
                "1", "path/1",
                CreateObject(1, null, "path/1"),
                CreateObject(2, null, "path/1"),
                CreateObject(3, null, "path/1")
            );
            SetupCampaignCollections(
                "1", "path/2",
                CreateObject(1, null, "path/2"),
                CreateObject(2, null, "path/2")
            );
            SetupCampaignCollections(
                "2", "path/1",
                CreateObject(2, null, "path/1")
            );
            Equal(3, (await Controller.Get("/campaigns/campaign1/path/1")).Count);
            Equal(2, (await Controller.Get("/campaigns/campaign1/path/2")).Count);
            Single(await Controller.Get("/campaigns/campaign2/path/1"));
        }

        [Fact]
        public async Task ReturnsLibraryDataInFolder()
        {
            SetupCampaigns(
                new CampaignDTO { Id = "1", Name = "lotr" }
            );
            SetupCampaignCollections(
                "1", "shire/hobbits",
                CreateObject(1, "lotr", "shire/hobbits", new JObject
                {
                    { "hp", 30 },
                    { "name", "Merry" }
                })
            );
            var results = await Controller.Get("/campaigns/lotr/shire/hobbits/");
            Single(results);
            var merry = results[0];
            Equal("1", merry.Id);
            Equal("shire/hobbits", merry.Path);
            Equal("lotr", merry.Campaign);
            Equal("character", merry.Collection);
        }

        [Fact]
        public async Task RecursionAndSpaces()
        {
            SetupCampaigns(
                new CampaignDTO { Id = "1", Name = "Tutorial Campaign" }
            );
            SetupCampaignCollections(
                "1",
                CreateFolder(1, "folder", "1", null)
            );

            var results = await Controller.Get("/", recursive: true);
            var expectation = JObject.Parse($@"{{
                campaigns: {{
                    Tutorial_Campaign: {{
                        folder: {{
                        }}
                    }}
                }},
                library: {{}},
                public: {{}}
            }}");
            LibraryTestHelper.VerifyFolderStructure(expectation, results);
        }
    }
}
