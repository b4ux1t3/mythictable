﻿using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using Xunit;
using static Xunit.Assert;

namespace MythicTable.Tests.Library.Controller
{
    public class LibraryControllerNameTests : LibraryControllerBase
    {
        [Fact]
        public async Task FoldersHaveDefaultNames()
        {
            SetupCampaignCollections(
                (string)null,
                CreateFolder(1, "folder1", null, null)
            );
            var results = await Controller.Get("/library/");
            Single(results);
            var folder = results[0];
            Equal("1", folder.Id);
            Equal("folder1", folder.Name);
            Equal("folder", folder.Collection);
        }

        [Fact]
        public async Task WillAttemptToFindNameField()
        {
            SetupCampaignCollections(
                (string)null, "shire/hobbits",
                CreateObject(1, null, "shire/hobbits", new JObject
                {
                    { "name", "Pippin" }
                })
            );
            var results = await Controller.Get("/library/shire/hobbits/");
            Single(results);
            var pippin = results[0];
            Equal("Pippin", pippin.Name);
        }

        [Fact]
        public async Task NameSearchIgnoresCase()
        {
            SetupCampaignCollections(
                (string)null, "shire/hobbits",
                CreateObject(1, null, "shire/hobbits", new JObject
                {
                    { "NAME", "Pippin" }
                })
            );
            var results = await Controller.Get("/library/shire/hobbits/");
            Single(results);
            var pippin = results[0];
            Equal("Pippin", pippin.Name);
        }
    }
}
