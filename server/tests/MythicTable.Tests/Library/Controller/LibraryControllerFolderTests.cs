﻿using MythicTable.Library.Controller;
using Xunit;

namespace MythicTable.Tests.Library.Controller
{
    public class LibraryFolderTests
    {
        [Theory]
        [InlineData("test")]
        [InlineData("test1")]
        [InlineData("test_1")]
        [InlineData("1")]
        [InlineData("folder-name")]
        public void ValidName(string name)
        {
            Assert.True(LibraryController.IsValidFolderName(name));
        }

        [Theory]
        [InlineData("")]
        [InlineData("/")]
        [InlineData("any/slashes/")]
        [InlineData("no spaces")]
        public void InvalidName(string name)
        {
            Assert.False(LibraryController.IsValidFolderName(name));
        }
    }
}
