﻿using MythicTable.Campaign.Data;
using MythicTable.Library.Controller;
using System.Threading.Tasks;
using Xunit;
using static Xunit.Assert;

namespace MythicTable.Tests.Library.Controller
{
    public class LibraryControllerMakeFolderTests : LibraryControllerBase
    {
        [Fact]
        public async Task FolderNamesMustBeValid()
        {
            const string folderName = "folder/name";
            const string path = "/library/";
            var exception = await ThrowsAsync<LibraryException>(() => Controller.MakeFolder(path, folderName));
            Equal($"Invalid folder name '{folderName}'", exception.Message);
        }

        [Fact]
        public async Task FailsIfFolderExists()
        {
            const string folderName = "folder-name";
            const string path = "/library/";
            SetupCampaignCollections(
                (string)null,
                CreateFolder(1, folderName, null, null)
            );
            var exception = await ThrowsAsync<LibraryException>(() => Controller.MakeFolder(path, folderName));
            Equal($"Folder '{folderName}' exists at '{path}'", exception.Message);
        }

        [Fact]
        public async Task MakingFolderCreateCollectionObject()
        {
            const string folderName = "folder-name";
            var folder = await Controller.MakeFolder("/library/", folderName);
            Equal(folderName, folder.Name);
            Equal("", folder.Path);
            Equal("folder", folder.Collection);

            VerifyCollectionCreated("folder", "", folderName);
        }

        [Fact]
        public async Task CannotCreateFoldersInNonExistentCampaigns()
        {
            var exception = await ThrowsAsync<LibraryException>(() => Controller.MakeFolder("/campaigns/foo", "folder-name"));
            Equal($"Cannot find campaign 'foo'", exception.Message);
        }

        [Fact]
        public async Task CannotCreateFoldersAtCampaignLevel()
        {
            var exception = await ThrowsAsync<LibraryException>(() => Controller.MakeFolder("/campaigns", "folder-name"));
            Equal("Cannot add folder at campaign level. Create a new campaign instead.", exception.Message);
        }

        [Fact]
        public async Task MakingFolderInCampaign()
        {
            const string folderName = "folder-name";
            SetupCampaigns(
                new CampaignDTO { Id = "777", Name = "foo" }
            );
            var folder = await Controller.MakeFolder("/campaigns/foo", folderName);
            Equal(folderName, folder.Name);
            Equal("", folder.Path);
            Equal("777", folder.Campaign);
            Equal("folder", folder.Collection);

            VerifyCollectionCreated("folder", "", folderName, "777");
        }

        [Fact]
        public async Task MakesAllFoldersRecursively()
        {
            const string folderName = "folder-name";
            var folder = await Controller.MakeFolder("/library/path/to/", folderName);
            Equal(folderName, folder.Name);
            Equal("path/to", folder.Path);
            Equal("folder", folder.Collection);

            VerifyCollectionCreated("folder", "", "path");
            VerifyCollectionCreated("folder", "path", "to");
            VerifyCollectionCreated("folder", "path/to", folderName);
        }

        [Fact]
        public async Task MakesAllFoldersRecursivelyInCampaigns()
        {
            const string folderName = "folder-name";
            SetupCampaigns(
                new CampaignDTO { Id = "777", Name = "foo" }
            );
            var folder = await Controller.MakeFolder("/campaigns/foo/path/to/", folderName);
            Equal(folderName, folder.Name);
            Equal("path/to", folder.Path);
            Equal("777", folder.Campaign);
            Equal("folder", folder.Collection);

            VerifyCollectionCreated("folder", "", "path", "777");
            VerifyCollectionCreated("folder", "path", "to", "777");
            VerifyCollectionCreated("folder", "path/to", folderName, "777");
        }
    }
}
