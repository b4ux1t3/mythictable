﻿using MythicTable.Library.Controller;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using Xunit;
using static Xunit.Assert;

namespace MythicTable.Tests.Library.Controller
{
    public class LibraryControllerMoveTests : LibraryControllerBase
    {
        [Fact]
        public async Task CannotMoveObjectToRoot()
        {
            const string objectId = "1";
            var exception = await ThrowsAsync<LibraryException>(() => Controller.Move(objectId, "/"));
            Equal("Cannot move objects to root", exception.Message);
        }

        [Fact]
        public async Task PathMustBeValid()
        {
            const string objectId = "1";
            var exception = await ThrowsAsync<LibraryException>(() => Controller.Move(objectId, "/shoes/"));
            Equal("Invalid path '/shoes/'", exception.Message);
        }

        [Fact]
        public async Task ObjectMustExist()
        {
            const string objectId = "1";
            var exception = await ThrowsAsync<LibraryException>(() => Controller.Move(objectId, "/library/shoes"));
            Equal($"Cannot find object '{objectId}' for user '{UserId}'", exception.Message);
        }

        [Fact]
        public async Task NoTrailingSlashRenames()
        {
            const string objectId = "1";
            SetupCollectionGet(objectId, null, null, null, new JObject
            {
                { "hp", "30" },
                { "name", "Pippin" },
            });
            await Controller.Move(objectId, "/library/merry");
            VerifyRename("1", "merry", null, null);
        }

        [Fact]
        public async Task MoveFailsIfNoChanges()
        {
            const string folder = "folder-name";
            const string path = "path/to/destination";
            const string objectId = "1";
            SetupCollectionGet(objectId, path, null, "folder", new JObject
            {
                { "_name", folder },
            });
            var exception = await ThrowsAsync<LibraryException>(() => Controller.Move(objectId, $"/library/{path}/"));
            Equal($"Cannot move '{objectId}'. No changes.", exception.Message);
        }

        [Fact]
        public async Task RenameFailsIfNoChanges()
        {
            const string folder = "folder-name";
            const string path = "path/to/destination";
            const string objectId = "1";
            SetupCollectionGet(objectId, path, null, "folder", new JObject
            {
                { "_name", folder },
            });
            var exception = await ThrowsAsync<LibraryException>(() => Controller.Move(objectId, $"/library/{path}/{folder}"));
            Equal($"Cannot move '{objectId}'. No changes.", exception.Message);
        }

        [Fact]
        public async Task RenameFolderFailsIfFolderExists()
        {
            const string folder = "folder-name";
            const string dest = "path/to/destination";
            const string objectId = "1";
            SetupCollectionGet(objectId, "path/to/origin", null, "folder", new JObject
            {
                { "_name", folder },
            });
            SetupCampaignCollections(
                null, dest,
                CreateFolder(2, folder, null, dest)
            );
            var exception = await ThrowsAsync<LibraryException>(() => Controller.Move(objectId, $"/library/{dest}/{folder}"));
            Equal($"Cannot rename folder because '{folder}' exists at '{dest}'", exception.Message);
        }
    }
}
