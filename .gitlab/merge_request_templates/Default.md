<!-- Title suggestion: Issue/Bug/Improvement #??: Short summary-->

## Description

<!-- Briefly describe what this MR is about. -->

## Checklist

- [ ] Tests
- [ ] Conventions
- [ ] Adds value

## SonarCloud Pages
- [Frontend](https://sonarcloud.io/dashboard?id=mythictable-frontend)
- [Backend](https://sonarcloud.io/project/configuration?id=mythictable-backend)
<!-- ## Areas of Interest
- link
- link
-->

<!-- ## Areas to Test
- [ ] Description
-->
